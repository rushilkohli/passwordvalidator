package validator;

/**
 *
 * @author rushil
 */


public class PasswordValidator {
    
    private String password;

    public String getPassword() {
        return password;
    }
    
    private static boolean checkLength(String password) {
            
        return password.length() >= 8;        
    }
    
    private static boolean checkSpecialChar(String password) {
        if((password.contains("@") || password.contains("$") || password.contains("+") || password.contains("!") 
              || password.contains("#") || password.contains("?") || password.contains("^") || password.contains("&")))
        {
            return true;
        }
        else {
            return false;
        }
    }
    
    private static boolean checkUpperCase(String password) {
       char pass;
       for(int i = 0; i < password.length(); i++) {
           pass = password.charAt(i);
           if(Character.isUpperCase(pass)) {
               return true;
           }
       }
       return false;
    }
    
    private static boolean checkDigit(String password) {
        if (!(password.contains("1") || password.contains("2") || password.contains("3") || password.contains("4") || password.contains("5")
             || password.contains("6") || password.contains("7") || password.contains("8") || password.contains("9") || password.contains("0")))
        {
            return true;
        }
        else {
            return false;
        }
    }
    
    public static boolean validatePassword(String password)
    {
        return checkLength(password) && checkSpecialChar(password) && checkUpperCase(password) && checkDigit(password);
        
    }
}
