package validator;

/**
 *
 * @author rushil
 */
public class PasswordSimulation {
    
    public static void main(String[] args) {
        
        String password1 = "123@Rushilkohli";
        if(PasswordValidator.validatePassword(password1) == true)
        {
            System.out.println("First is Correct");
        }
        else
        {
            System.out.println("First is Incorrect");
        }
        
        String password2 = "12345$Rushilkohli";
        if(PasswordValidator.validatePassword(password2) == true)
        {
            System.out.println("Second is Correct");
        }
        else
        {
            System.out.println("Second is Incorrect");
        }
        
        String password3 = "987#Rushilkohli";
        if(PasswordValidator.validatePassword(password3) == true)
        {
            System.out.println("Third is Correct");
        }
        else
        {
            System.out.println("Third is Incorrect");
        }
        
        String password4 = "785&Rushilkohli";
        if(PasswordValidator.validatePassword(password4) == true)
        {
            System.out.println("Fourth is Correct");
        }
        else
        {
            System.out.println("Fourth is Incorrect");
        }
    }
}
