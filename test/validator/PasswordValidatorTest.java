package validator;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rushil
 */
public class PasswordValidatorTest {
    
    public PasswordValidatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    @Test
    public void testValidatePasswordLengthRegular   () {
        System.out.println("validatePassword");
        String password = "abcdefghij";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password does not meet the requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordLengthException() {
        System.out.println("validatePassword length exception");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password does not meet the requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordLengthBoundaryIn() {
        System.out.println("validatePassword length boundary in");
        String password = "abcdefgh";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password does not meet the requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordLengthBoundaryOut() {
        System.out.println("validatePassword length boundary Out");
        String password = "abcdefg";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password does not meet the requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordSpecialCharRegular() {
        System.out.println("validatePassword special char regular");
        String password = "abcdef@ghij";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password does not meet the special char requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
    public void testValidatePasswordSpecialCharException() {
        System.out.println("validatePassword special char exception");
        String password = "abcdefghi";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password does not meet the special char requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
    public void testValidatePasswordSpecialCharBoundaryIn() {
        System.out.println("validatePassword special char boundary in");
        String password = "abcdef@ghij";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password does not meet the special char requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
    public void testValidatePasswordSpecialCharBoundaryOut() {
        System.out.println("validatePassword special char boundary out");
        String password = "abcdefghij";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password does not meet the special char requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
    public void testValidatePasswordCheckUpperCaseRegular() {
        System.out.println("validatePassword upper case regular");
        String password = "Abcdef@!>ghij";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password does not meet the Upper case requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
    public void testValidatePasswordCheckUpperCaseException() {
        System.out.println("validatePassword upper case exception");
        String password = "abcdef@!>ghij";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password does not meet the Upper case requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
    public void testValidatePasswordCheckUpperCaseBoundaryIn() {
        System.out.println("validatePassword upper case boundary in");
        String password = "Abcdef@!>ghij";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password does not meet the Upper case requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
    public void testValidatePasswordCheckUpperCaseBoundaryOut() {
        System.out.println("validatePassword upper case boundary out");
        String password = "abcdef@!>ghij";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password does not meet the Upper case requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
    public void testValidatePasswordCheckDigitRegular() {
        System.out.println("validatePassword digit regular");
        String password = "Abcdef@!>ghij123";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password does not meet the Digit requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
    public void testValidatePasswordCheckDigitException() {
        System.out.println("validatePassword digit exception");
        String password = "Abcdef@!>ghij";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password does not meet the Digit requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
    public void testValidatePasswordCheckDigiBoundaryIn() {
        System.out.println("validatePassword digit boundary in");
        String password = "Abcdef@!>ghij123";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password does not meet the Digit requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
    
    public void testValidatePasswordCheckDigitBoundaryOut() {
        System.out.println("validatePassword digit boundary out");
        String password = "Abcdef@!>ghij";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password does not meet the Digit requirements", expResult, result);
        //fail("The test case is a prototype.");
    }
}
